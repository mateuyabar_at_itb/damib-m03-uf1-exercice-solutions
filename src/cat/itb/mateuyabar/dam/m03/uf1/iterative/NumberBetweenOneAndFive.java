package cat.itb.mateuyabar.dam.m03.uf1.iterative;

import java.util.Scanner;

public class NumberBetweenOneAndFive {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        int value;
        do {
            System.out.println("introdueix un número de l'1 al 5");
            value = scanner.nextInt();
        } while(value<1 || value>5);

        System.out.println("El número introduït: "+value);


    }
}
