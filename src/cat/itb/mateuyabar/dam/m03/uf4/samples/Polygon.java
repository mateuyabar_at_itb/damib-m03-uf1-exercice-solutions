package cat.itb.mateuyabar.dam.m03.uf4.samples;

public interface Polygon {
    double getArea();
}
