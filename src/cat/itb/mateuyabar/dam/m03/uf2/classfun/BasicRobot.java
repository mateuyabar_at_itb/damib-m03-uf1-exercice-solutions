package cat.itb.mateuyabar.dam.m03.uf2.classfun;

import java.util.Scanner;

public class BasicRobot {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Robot robot = new Robot(0,0,1);
        String operation = scanner.next();
        while(operation!="END"){
            executeOperation(robot, operation);
            operation = scanner.next();
        }
    }

    private static void executeOperation(Robot robot, String operation) {
        switch (operation) {
            case "DALT":
                robot.goUp();
                break;
            case "BAIX":
                robot.goDown();
                break;
            case "DRETA":
                robot.goRight();
                break;
            case "ESQUERRA":
                robot.goLeft();
                break;
            case "ACCELERAR":
                robot.accelerate();
                break;
            case "DISMINUIR":
                robot.slowDown();
                break;
            case "POSICIO":
                robot.printPosition();
                break;
            case "VELOCITAT":
                robot.printSpeed();
                break;
        }
    }
}
