package cat.itb.mateuyabar.dam.m03.uf3.project.ui;

import cat.itb.mateuyabar.dam.m03.uf3.project.data.League;
import cat.itb.mateuyabar.dam.m03.uf3.project.storage.LeagueStorage;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class StorageMenu {
    /**
     * Returns the league if a new league has been loaded, or null in any other case
     * @param scanner
     * @param league
     * @return
     */
    public static League displayStorageMenu(Scanner scanner, League league) {
        System.out.printf("display tems options");
        int operation = scanner.nextInt();
        switch (operation) {
            case 1:
                saveLeague(scanner, league);
                return null;
            case 2:
                return loadLeague(scanner);
        }
        return null;
    }

    private static League loadLeague(Scanner scanner) {
        String pathString = scanner.nextLine();
        Path path = Paths.get(pathString);
        return LeagueStorage.load(path);
    }

    private static void saveLeague(Scanner scanner, League league) {
        String pathString = scanner.nextLine();
        Path path = Paths.get(pathString);
        LeagueStorage.store(path, league);
    }


}
