package cat.itb.mateuyabar.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class NextSecond {
    public static void main(String[] args) {
        // Ask user for hour minutes seconds
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdueix hores, minuts i segons");
        int hour = scanner.nextInt();
        int minute = scanner.nextInt();
        int second = scanner.nextInt();

        // calculate next second
        if(second!=59){
            second++;
        } else {
            second = 0;
            if(minute!=59){
                minute++;
            } else {
                minute = 0;
                if(hour!=23){
                    hour++;
                } else {
                    hour = 0;
                }
            }
        }

        String hourString = timeToString(hour);
        String minuteString = timeToString(minute);
        String secondString = timeToString(second);

        // print result
        System.out.println(hourString + ":" + minuteString + ":" + secondString);
    }

    private static String timeToString(int hour) {
        if(hour<10)
            return "0"+hour;
        else
            return ""+hour;
    }
}
