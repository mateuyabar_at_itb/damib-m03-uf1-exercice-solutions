package cat.itb.mateuyabar.dam.m03.uf5.exercices.func;

public class Embassator {
    String name;
    String surname;
    Country country;

    public Embassator(String name, String surname, Country country) {
        this.name = name;
        this.surname = surname;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Country getCountry() {
        return country;
    }

    public String getCountryName() {
        return country.getName();
    }
}
