package cat.itb.mateuyabar.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class ChooseYourOwnAdventure {
    private static void waitToDaylight() {
        System.out.println("S'ha fet de dia. Tens molta gana");
        System.out.println("Que vols fer?");
        System.out.println("1. Espero");
        System.out.println("2. Vaig a cercar menjar");
        Scanner scanner = new Scanner(System.in);
        int userOption = scanner.nextInt();
        if(userOption==1){
            deadByStarvation();
        } else {
            goToFindFood();
        }
    }

    private static void deadByStarvation() {
        System.out.println("Has mort de gana");
    }

    private static void goToFindFood() {
        System.out.println("Hi ha una palmera amb cocos");
        System.out.println("Que vols fer?");
        System.out.println("1. No hi pujo, és perillós");
        System.out.println("2. Hi pujo");
        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        if(option==1)
            deadByStarvation();
        else
            win();
    }

    private static void win() {
        System.out.println("Desde dalt de la palmera veus un vaixell. Estàs salvat!");
    }


    public static void main(String[] args) {
        System.out.println("Et despertes a una illa de nit. L'últim que recordes és ofegar-se.");
        System.out.println("Que vols fer?");
        System.out.println("1. Espero");
        System.out.println("2. Vaig a cercar menjar");
        Scanner scanner = new Scanner(System.in);
        int userOption = scanner.nextInt();
        if(userOption==1){
            waitToDaylight();
        } else {
            nightHunter();
        }
    }

    private static void nightHunter() {
        System.out.println("És de nit, no veus res.");
        System.out.println("Has mort de gana");
    }


}
    
