package cat.itb.mateuyabar.dam.m03.uf1.iterative;

import java.util.Scanner;

public class LetsCount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int maxValue = scanner.nextInt();

        int i = 1;
        while(i<=maxValue) {
            System.out.print(i);
            i++;
        }
    }
}
