package cat.itb.mateuyabar.dam.m03.uf1.data;

import java.util.Scanner;

/**
 * L'usuari entra un preu original i el preu actual i s'imprimeix el descompte (en %).
 */
public class CalculateDiscount {
    public static void main(String[] args) {
        // ask user current price and normal price
        Scanner scanner = new Scanner(System.in);
        double currentPrice = scanner.nextDouble();
        double previousPrice = scanner.nextDouble();

        // calculate discount
        // - Subtract the final price from the original price.
        double difference = previousPrice - currentPrice;
        // Divide this number by the original price.
        double aux = difference / previousPrice;
        // Finally, multiply the result by 100.
        double discount = aux * 100;

        // print discount
        System.out.println(discount);
    }
}
