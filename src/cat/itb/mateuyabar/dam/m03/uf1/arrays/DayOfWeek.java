package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Scanner;

public class DayOfWeek {
    public static void main(String[] args) {
        String[] dayNames = {"dilluns", "dimarts", "dimecres",
                "dijous", "divendres", "dissabte", "diumenge"};

        Scanner scanner = new Scanner(System.in);
        int dayOfWeek = scanner.nextInt();
        String dayName = dayNames[dayOfWeek];
        System.out.println(dayName);
    }
}
