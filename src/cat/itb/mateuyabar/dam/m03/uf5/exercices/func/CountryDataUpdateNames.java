package cat.itb.mateuyabar.dam.m03.uf5.exercices.func;

import java.util.List;
import java.util.Scanner;

public class CountryDataUpdateNames {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Country> countryList = CountryDataSortByName.readCountries(scanner);

        countryList.stream()
                .map(CountryDataUpdateNames::toUpperCase)
                .forEach(System.out::println)
        ;
    }

    private static Country toUpperCase(Country country) {
        country.toUpperCase();
        return country;
    }


}
