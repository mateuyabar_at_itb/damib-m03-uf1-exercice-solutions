package cat.itb.mateuyabar.dam.m03.uf1.iterative.practice;

public class Test {
    public static void main(String[] args) {
        int[] values = {3,6,8,9};

        for(int value:values){
            System.out.println(value);
        }
    }
}
