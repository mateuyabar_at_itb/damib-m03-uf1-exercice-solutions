package cat.itb.mateuyabar.dam.m03.uf5.exercices.func;

import cat.itb.mateuyabar.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class LamdaSample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> values = IntegerLists.readIntegerList(scanner);

        values.removeIf(integer -> integer%10==3);
        values.sort((v1, v2) -> v2-v1);
        values.forEach(integer -> System.out.println(integer));
    }

}
