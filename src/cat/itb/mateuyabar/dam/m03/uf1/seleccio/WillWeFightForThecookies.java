package cat.itb.mateuyabar.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class WillWeFightForThecookies {
    public static void main(String[] args) {
        // ask for number of people and cookies

        Scanner scanner = new Scanner(System.in);
        System.out.println("Quantes persones sou?");
        int peopleCount = scanner.nextInt();
        System.out.println("Quantes galetes teniu?");
        int cookiesCount = scanner.nextInt();

        boolean canDivide = cookiesCount % peopleCount == 0;
        String message;
        if(canDivide){
            message = "Let's Eat!";
        } else {
            message = "Let's Fight";
        }
        System.out.println(message);
    }
}
