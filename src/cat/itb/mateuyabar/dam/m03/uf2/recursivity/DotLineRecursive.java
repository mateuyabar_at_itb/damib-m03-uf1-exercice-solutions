package cat.itb.mateuyabar.dam.m03.uf2.recursivity;

public class DotLineRecursive {
    public static String writeDots(int dots){
        if(dots==0)
            return "";
        return "."+writeDots(dots-1);
    }
}
