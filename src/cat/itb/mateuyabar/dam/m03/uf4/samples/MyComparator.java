package cat.itb.mateuyabar.dam.m03.uf4.samples;

import java.util.Comparator;

public class MyComparator implements Comparator<Integer> {
    @Override
    public int compare(Integer i1, Integer i2) {
        return i2-i1;
    }
}
