package cat.itb.mateuyabar.dam.m03.uf4;

public enum Grade {
    FAILED, PASSED, GOOD, NOTABLE, EXCELLENT
}
