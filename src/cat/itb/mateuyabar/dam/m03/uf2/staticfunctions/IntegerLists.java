package cat.itb.mateuyabar.dam.m03.uf2.staticfunctions;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IntegerLists {
    public static List<Integer> readIntegerList(Scanner scanner){
        List<Integer> list = new ArrayList<>();
        int value = scanner.nextInt();
        while(value!=-1) {
            list.add(value);
            value = scanner.nextInt();
        }
        return list;
    }

    public static List<Integer> readFromPath(Path path) throws IOException {
        List<Integer> list = new ArrayList<>();
        Scanner filesSanner = new Scanner(path);
        while(filesSanner.hasNext()){
            int speed = filesSanner.nextInt();
            list.add(speed);
        }
        return list;
    }

    /**
     * Calculates the min of a list
     * @param list list to serach
     * @return min value
     */
    public static int min(List<Integer> list){
        int result = list.get(0);
        for(int value : list){
            result = Math.min(result, value);
        }
        return result;
    }

    public static int max(List<Integer> list) {
        int result = list.get(0);
        for(int value : list){
            result = Math.max(result, value);
        }
        return result;
    }

    public static double avg(List<Integer> list) {
        return (double)sum(list)/list.size();
    }

    public static int sum(List<Integer> list) {
        int sum = 0;
        for(int value: list){
            sum+=value;
        }
        return sum;
    }
}
