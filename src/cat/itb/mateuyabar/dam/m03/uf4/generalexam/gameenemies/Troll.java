package cat.itb.mateuyabar.dam.m03.uf4.generalexam.gameenemies;

public class Troll extends Enemy{
    int resistance;

    public Troll(String name, int livePoints, int resistance) {
        super(name, livePoints);
        this.resistance = resistance;
    }

    @Override
    public void resolveAttack(int strength) {
        subtractLivePoints(Math.max(0, strength-resistance));
    }
}
