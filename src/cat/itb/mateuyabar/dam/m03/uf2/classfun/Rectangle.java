package cat.itb.mateuyabar.dam.m03.uf2.classfun;

public class Rectangle implements Comparable<Rectangle>{
    double width;
    double height;

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getArea() {
        return height*width;
    }

    @Override
    public int compareTo(Rectangle rectangle) {
        return (int) (getArea()-rectangle.getArea());
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }
}
