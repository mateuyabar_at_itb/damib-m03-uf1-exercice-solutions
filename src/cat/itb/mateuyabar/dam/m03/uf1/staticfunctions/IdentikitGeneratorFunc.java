package cat.itb.mateuyabar.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class IdentikitGeneratorFunc {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String hairText = scanner.nextLine();
        String eyesText = scanner.nextLine();
        String noseText = scanner.nextLine();
        String mouthText = scanner.nextLine();

        String hair = drawHair(hairText);
//        String eyes = drawEyes(eyesText);
//        String nose = drawNose(noseText);
//        String mouth = drawMouth(mouthText);

        System.out.println(hair);
//        System.out.println(eyes);
//        System.out.println(nose);
//        System.out.println(mouth);

    }

    private static String drawHair(String hairText) {
        switch (hairText){
            case "arrissats":
                return "@@@@@";
            case "llisos":
                return "VVVVV";
            case "pentinats":
                return "XXXXX";
            default:
                return "ERROR";
        }
    }
}
