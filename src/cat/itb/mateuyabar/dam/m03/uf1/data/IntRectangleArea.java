package cat.itb.mateuyabar.dam.m03.uf1.data;

import java.util.Scanner;

/**
 * Calcula l'area d'un rectangle (l'usuari introdueix la llargada dels dos costats en valor enter).
 */
public class IntRectangleArea {
    public static void main(String[] args) {
        //ask user length and width
        Scanner scanner = new Scanner(System.in);
        int height = scanner.nextInt();
        int width = scanner.nextInt();

        // area is height * width
        int area = rectagleArea(width, height);

        // print area
        System.out.println(area);
    }

    private static int rectagleArea(int width, int height) {
        return height * width;
    }
}
