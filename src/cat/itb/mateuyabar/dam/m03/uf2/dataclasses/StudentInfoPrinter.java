package cat.itb.mateuyabar.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StudentInfoPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<School> schools = readSchools(scanner);
        List<Student> students = readStudents(scanner, schools);
        printStudents(students);
    }

    private static void printStudents(List<Student> students) {
        for(Student student: students){
            System.out.println(student);
        }
    }

    private static List<Student> readStudents(Scanner scanner, List<School> schoolList) {
        List<Student> list = new ArrayList<>();
        int size = scanner.nextInt();
        for(int i=0; i<size; ++i){
            Student student = Student.readStudent(scanner, schoolList);
            list.add(student);
        }
        return list;
    }

    private static List<School> readSchools(Scanner scanner) {
        List<School> schools = new ArrayList<>();
        int size = scanner.nextInt();
        scanner.nextLine();
        for(int i=0; i<size; ++i){
            School school = School.readSchool(scanner);
            schools.add(school);
        }
        return schools;
    }
}
