package cat.itb.mateuyabar.dam.m03.uf4.vehicles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VehicleComparableApp {
    public static void main(String[] args) {
        VehicleBrand brand = new VehicleBrand("name", "Fsd");
        List<VehicleModel> list = new ArrayList<>();
        list.add(new BicycleModel("dsasd", brand, 6));
        list.add(new BicycleModel("rew", brand, 7));
        list.add(new ScooterModel("ra", brand, 6.3));
        list.add(new ScooterModel("r3a", brand, 4.6));
        list.add(new BicycleModel("af34", brand, 1));

        Collections.sort(list);
        Collections.sort(list, (v1, v2) -> v1.name.compareTo(v2.name));
    }
}
