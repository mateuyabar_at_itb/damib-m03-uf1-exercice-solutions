package cat.itb.mateuyabar.dam.m03.uf1.seleccio.exam;

import java.util.Scanner;

public class ThreeCardPoker {
    public static void main(String[] args) {
        int card1 = 1;
        int card2 = 2;
        int card3 = 3;

        if(card1==card2 && card2 == card3){
            System.out.printf("Trio");
        } else if(card1==card2 || card2 == card3 || card1 == card3){
            System.out.println("Parella");
        } else if(card1 == card2-1 && card2 == card3-1){
            System.out.println("Escala");
        } else {
            System.out.println("Número alt");
        }

    }
}
