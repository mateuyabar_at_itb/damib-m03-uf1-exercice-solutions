package cat.itb.mateuyabar.dam.m03.uf3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

public class IWasHere {
    public static void main(String[] args) throws IOException {
        String homePath = System.getProperty("user.home");
        String pathString = "i_was_here.txt";
        Path path = Paths.get(homePath, pathString);
        String date = LocalDateTime.now().toString();
        String text = String.format("I Was Here: %s%n", date);
        Files.writeString(path, text, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
    }
}
