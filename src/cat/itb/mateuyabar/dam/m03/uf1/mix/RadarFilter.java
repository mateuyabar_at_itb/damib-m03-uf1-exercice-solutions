package cat.itb.mateuyabar.dam.m03.uf1.mix;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RadarFilter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> speeds = new ArrayList<>();

        int speed = scanner.nextInt();
        int counter = 0;
        while (speed!=-1){
            if(speed>90)
                counter++;
            speed = scanner.nextInt();
        }
        System.out.println(counter);

    }
}
