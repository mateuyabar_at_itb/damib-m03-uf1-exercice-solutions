package cat.itb.mateuyabar.dam.m03.uf4.generalexam.gameenemies;

public class Goblin extends Enemy{

    public Goblin(String name, int livePoints) {
        super(name, livePoints);
    }

    @Override
    public void resolveAttack(int strength) {
        if(strength<4)
            subtractLivePoints(1);
        else
            subtractLivePoints(5);
    }
}
