package cat.itb.mateuyabar.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

import static cat.itb.mateuyabar.dam.m03.uf1.staticfunctions.HowBigIsMyPizzaFunc.circleAreaFromDiameter;

public class BiggerPizzaFunc {
    public static void main(String[] args) {
        // Ask user for diameter
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdueix el diametre de la pizza");
        double diameter = scanner.nextDouble();

        // Ask user for rectangle sides
        System.out.println("Introdueix els dos costats de la pizza");
        double width = scanner.nextDouble();
        double height = scanner.nextDouble();

        boolean result = isRoundedBigger(diameter, width, height);

        System.out.println(result);

    }

    public static boolean isRoundedBigger(double diameter, double width, double height) {
        double roundedArea = circleAreaFromDiameter(diameter);
        double rectangleAreaPizza = rectangleArea(width, height);
        return roundedArea > rectangleAreaPizza;
    }

    private static double rectangleArea(double width, double height) {
        return width * height;
    }
}
