package cat.itb.mateuyabar.dam.m03.uf4.motto;

public class VolleyTeam extends Team{
    String color;

    public VolleyTeam(String name, String motto, String color) {
        super(name, motto);
        this.color = color;
    }

    @Override
    public void shoutMotto() {
        System.out.println(motto+" "+color);
    }
}
