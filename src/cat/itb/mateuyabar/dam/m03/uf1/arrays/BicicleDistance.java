package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Scanner;

public class BicicleDistance {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double speed = scanner.nextDouble();

        for(int i = 1; i<=10; ++i){
            System.out.print(speed*i);
        }
    }
}
