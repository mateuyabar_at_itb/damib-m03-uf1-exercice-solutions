package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ArraySumValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] values = ArrayReader.scannerReadIntArray(scanner);

        int sum = 0;
        for(int value : values){
            sum += value;
        }

        System.out.println(sum);
    }
}
