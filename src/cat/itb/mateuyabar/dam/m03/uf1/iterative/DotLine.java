package cat.itb.mateuyabar.dam.m03.uf1.iterative;

import java.util.Scanner;

public class DotLine {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int dotCount = scanner.nextInt();

        for(int i = 0; i<dotCount; i++) {
            System.out.print(".");
        }
    }
}
