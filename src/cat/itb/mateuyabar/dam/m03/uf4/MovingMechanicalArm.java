package cat.itb.mateuyabar.dam.m03.uf4;

public class MovingMechanicalArm extends MechanicalArm{
    double position;

    public void move(double movement){
        if(turnedOn)
            position = Math.max(0,position+movement);
    }

    @Override
    public String toString() {
        return "MovingMechanicalArm{" +
                "openAngle=" + openAngle +
                ", turnedOn=" + turnedOn +
                ", altitude=" + altitude +
                ", position=" + position +
                '}';
    }
}
