package cat.itb.mateuyabar.dam.m03.uf4.samples;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ComparatorApp {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(54,874,88,4864,898,64,384,1,64,6846,8468,74896,64,864,313,4);
        Collections.sort(list, (i1, i2) -> i2-i1);
        System.out.println(list);
    }
}
