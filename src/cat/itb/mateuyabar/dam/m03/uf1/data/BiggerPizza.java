package cat.itb.mateuyabar.dam.m03.uf1.data;

import java.util.Scanner;

public class BiggerPizza {
    public static void main(String[] args) {
        // Ask user for diameter
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdueix el diametre de la pizza");
        double diameter = scanner.nextDouble();

        // Ask user for rectangle sides
        System.out.println("Introdueix els dos costats de la pizza");
        double width = scanner.nextDouble();
        double height = scanner.nextDouble();

        // calculate rounded pizza size
        double radius = diameter / 2;
        double pi = 3.1415926535898;
        double roundedSize =  pi * radius * radius;

        // calculate rectangle pizza size
        double rectangleSize = width * height;

        // compare sizes
        boolean roundedBigger = roundedSize > rectangleSize;

        // print results
        System.out.println("És més gran la pizza rodona?");
        System.out.println(roundedBigger);
    }
}
