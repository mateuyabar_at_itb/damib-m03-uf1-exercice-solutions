package cat.itb.mateuyabar.dam.m03.uf4;

import java.util.Arrays;
import java.util.List;

public class PlantWaterMock implements PlantWater{
    @Override
    public List<Double> getHumidityRecord() {
        return Arrays.asList(new Double[]{1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.3});
    }

    @Override
    public void startWatterSystem() {
        System.out.println("START WATER SYSTEM");
    }
}
