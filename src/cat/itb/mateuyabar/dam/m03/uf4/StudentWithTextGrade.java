package cat.itb.mateuyabar.dam.m03.uf4;

public class StudentWithTextGrade {
    String name;
    Grade grade;

    public StudentWithTextGrade(String name, Grade grade) {
        this.name = name;
        this.grade = grade;
    }
}
