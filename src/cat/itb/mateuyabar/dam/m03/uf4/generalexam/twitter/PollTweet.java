package cat.itb.mateuyabar.dam.m03.uf4.generalexam.twitter;

import java.util.List;

public class PollTweet extends Tweet{
    List<Choice> choices;

    public PollTweet(String user, String date, String text, List<Choice> choices) {
        super(user, date, text);
        this.choices = choices;
    }

    public void vote(int index){
        choices.get(index).vote();
    }

    @Override
    public void print() {
        super.print();
        int totalVotes = getTotalVotes();
        for (Choice c : choices){
            System.out.printf("- (%d/%d) %s\n", c.getVotes(), totalVotes, c.getText());
        }
    }

    private int getTotalVotes() {
        int count = 0;
        for(Choice choice:choices){
            count+=choice.getVotes();
        }
        return count;
    }
}
