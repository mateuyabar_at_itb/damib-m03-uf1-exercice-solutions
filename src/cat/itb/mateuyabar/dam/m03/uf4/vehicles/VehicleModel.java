package cat.itb.mateuyabar.dam.m03.uf4.vehicles;

public class VehicleModel implements Comparable<VehicleModel> {
    String name;
    VehicleBrand brand;

    public VehicleModel(String name, VehicleBrand brand) {
        this.name = name;
        this.brand = brand;
    }

    @Override
    public int compareTo(VehicleModel vehicleModel) {
        return name.compareTo(vehicleModel.name);
    }
}
