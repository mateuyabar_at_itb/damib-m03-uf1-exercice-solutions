package cat.itb.mateuyabar.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StrangeOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = new ArrayList<>();

        boolean addToStart = true;

        int value = scanner.nextInt();
        while(value!=-1){
            // DO WORK
            if(addToStart)
                list.add(0,value);
            else
                list.add(value);

            // PREPARE FOR NEXT
            addToStart = !addToStart;
            value = scanner.nextInt();
        }
        System.out.println(list);


    }
}
