package cat.itb.mateuyabar.dam.m03.uf4.generalexam.gameenemies;

public class Zombie extends Enemy{
    String noise;


    public Zombie(String name, int livePoints, String noise) {
        super(name, livePoints);
        this.noise = noise;
    }

    @Override
    public void resolveAttack(int strength) {
        System.out.println(noise);
        subtractLivePoints(strength);

    }
}
