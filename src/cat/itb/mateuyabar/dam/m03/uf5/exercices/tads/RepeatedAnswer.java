package cat.itb.mateuyabar.dam.m03.uf5.exercices.tads;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class RepeatedAnswer {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Set<String> words = new HashSet<>();
        String word = scanner.nextLine();
        while(!word.equals("END")){
            boolean added = words.add(word);
            if(!added)
                System.out.println("MEEEC!");
            word = scanner.nextLine();
        }
    }
}
