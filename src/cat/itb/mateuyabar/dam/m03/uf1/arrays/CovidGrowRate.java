package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Scanner;

public class CovidGrowRate {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] values = ArrayReader.scannerReadIntArray(scanner);
        // double[] rates = new double[values.length-1];

        for (int i = 1; i<values.length; ++i){
            double current = values[i];
            double previous = values[i-1];
            double rate = current/previous;
            // rates[i-1] = rate;
            System.out.println(rate);
        }
        // System.out.println(rates);

    }
}
