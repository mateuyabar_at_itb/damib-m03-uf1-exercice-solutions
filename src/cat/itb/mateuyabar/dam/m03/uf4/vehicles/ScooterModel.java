package cat.itb.mateuyabar.dam.m03.uf4.vehicles;

public class ScooterModel extends VehicleModel{
    double power;

    public ScooterModel(String name, VehicleBrand brand, double power) {
        super(name, brand);
        this.power = power;
    }

    @Override
    public String toString() {
        return "ScooterModel{" +
                "power=" + power +
                ", name='" + name + '\'' +
                ", brand=" + brand +
                '}';
    }
}
