package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Scanner;

public class CapICuaValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] values = ArrayReader.scannerReadIntArray(scanner);

        boolean isCapICua = true;
        for(int i=0; i<values.length/2; ++i){
            int cap = values[i];
            int cua = values[values.length-1-i];
            if(cap!=cua){
                isCapICua = false;
                break;
            }
        }
        if(isCapICua)
            System.out.printf("cap i cua");
    }
}
