package cat.itb.mateuyabar.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class WhichBigger {
    public static void main(String[] args) {
        // Ask for two numbers
        Scanner scanner = new Scanner(System.in);
        int value1 = scanner.nextInt();
        int value2 = scanner.nextInt();

        // compare them
        boolean firstBigger = value1 > value2;

        // find bigger
        int result;
        if(firstBigger){
            result = value1;
        } else {
            result = value2;
        }

        // print result
        System.out.println(result);
    }
}
