package cat.itb.mateuyabar.dam.m03.uf1.data;

import java.util.Scanner;

/**
 * L'usuari escriu un enter i s'imprimeix true si existeix un bitllet d'euros amb la quantitat entrada, false en qualsevol altre cas.
 */
public class IsValidNote {
    public static void main(String[] args) {
        System.out.println(0.12345678901234567890);
        System.out.println(0.12345678901234567890f);
        System.out.println(123_456_789_123_456_789.0);
        System.out.println(123_456_789_123_456_789.0f);

        // request user integer
        Scanner scanner = new Scanner(System.in);
        int noteNumber = scanner.nextInt();

        // check if is a note
        // number in 5, 10, 20, 50, 100, 200, 500
        boolean is5Note = noteNumber == 5;
        boolean is10Note = noteNumber == 10;
        boolean is20Note = noteNumber == 20;
        boolean is50Note = noteNumber == 50;
        boolean is100Note = noteNumber == 100;
        boolean is200Note = noteNumber == 200;
        boolean is500Note = noteNumber == 500;

        boolean isNote = is5Note || is10Note || is20Note || is50Note || is100Note || is200Note || is500Note;

        // print result
        System.out.println(isNote);
    }
}
