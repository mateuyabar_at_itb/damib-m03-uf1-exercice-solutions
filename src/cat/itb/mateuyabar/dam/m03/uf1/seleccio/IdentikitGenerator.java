package cat.itb.mateuyabar.dam.m03.uf1.seleccio;

import java.util.Scanner;

/**
 *
 *
 * El nas pot ser aixafat ..0.., arromangat ..C.. o aguilenc ..V...
 * La boca pot ser normal .===., bigoti .∼∼∼. o dents-sortides .www..
 */
public class IdentikitGenerator {
    public static void main(String[] args) {
        // ask user for person details
        Scanner scanner = new Scanner(System.in);

        String hair = scanner.nextLine();
        String eyes = scanner.nextLine();
        String nose = scanner.nextLine();
        String mouth = scanner.nextLine();

        // calculate each of the face attributes
        //arrissats @@@@@, llisos VVVVV o pentinats XXXXX.
        String hairImage;
        switch (hair){
            case "arrissats":
                hairImage = "@@@@@";
                break;
            case "llisos":
                hairImage = "VVVVV";
                break;
            case "pentinats":
                hairImage = "XXXXX";
                break;
            default:
                hairImage = "ERROR";
        }

        // Els ulls poden ser  ,   o  ".
        String eyesImage;
        switch (eyes){
            case "aclucats":
                eyesImage = ".-.-.";
                break;
            case "rodons":
                eyesImage = ".o-o.";
                break;
            case "estrellats":
                eyesImage = "_.*-_.";
                break;
            default:
                eyesImage = "ERROR";
        }
        // TODO calculate nose and mouth

        // print face
        System.out.println(hairImage);
        System.out.println(eyesImage);
    }
}
