package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Arrays;

public class AddValuesToArray {
    public static void main(String[] args) {
        float[] values = new float[50];

        values[0] = 31;
        values[1] = 56;
        values[19] = 12;
        values[49] = 79;

        System.out.println(Arrays.toString(values));
    }
}
