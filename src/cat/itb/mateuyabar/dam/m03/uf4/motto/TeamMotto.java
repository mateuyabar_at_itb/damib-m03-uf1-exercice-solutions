package cat.itb.mateuyabar.dam.m03.uf4.motto;

import java.util.ArrayList;
import java.util.List;

public class TeamMotto {
    public static void main(String[] args) {
        List<Team> teamList = new ArrayList<>();
        teamList.add(new BasketballTeam("Mosques", "Bzzzanyarem"));
        teamList.add(new VolleyTeam("Dragons", "Grooarg", "Verd"));
        teamList.add(new GolfTeam("Abelles", "Piquem Fort", "Mar Pi"));

        shoutMottos(teamList);
    }

    public static void shoutMottos(List<Team> teams){
        for(Team team: teams)
            team.shoutMotto();
    }
}
