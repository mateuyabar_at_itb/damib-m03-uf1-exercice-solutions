package cat.itb.mateuyabar.dam.m03.uf4;

import cat.itb.mateuyabar.dam.m03.uf2.classfun.Rectangle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TestRec {
    public static void main(String[] args) {
        List<Rectangle> rectangleList = new ArrayList<>();
        rectangleList.add(new Rectangle(45,2));
        rectangleList.add(new Rectangle(45,232));
        rectangleList.add(new Rectangle(45,243));
        rectangleList.add(new Rectangle(425,22));
        rectangleList.add(new Rectangle(4,21));
        rectangleList.add(new Rectangle(5,22));

        Collections.sort(rectangleList);

        System.out.println(rectangleList);

    }
}
