package cat.itb.mateuyabar.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class HowBigIsMyPizzaFunc {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double pizzaDiameter = scanner.nextDouble();

        double pizzaSize = circleAreaFromDiameter(pizzaDiameter);

        System.out.println(pizzaSize);
    }

    public static double circleAreaFromDiameter(double pizzaDiameter) {
        double radius = pizzaDiameter / 2;
        return Math.PI * radius * radius;
    }
}
