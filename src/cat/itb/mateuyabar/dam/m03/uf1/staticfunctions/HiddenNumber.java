package cat.itb.mateuyabar.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class HiddenNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Quin numero he pensat?");
        int number = scanner.nextInt();

        int computerNumber = (int) (Math.random()*3+1);
        System.out.println(computerNumber);
        if(number==computerNumber)
            System.out.println("L'has encertat");
        else
            System.out.println("No l'has encertat");
    }
}
