package cat.itb.mateuyabar.dam.m03.uf4;

public class PlantWaterSystemApp {
    public static void main(String[] args) {
        PlantWaterMock plantWaterMock = new PlantWaterMock();

        PlantWaterControl plantWaterControl = new PlantWaterControl(plantWaterMock);
        plantWaterControl.waterIfNeeded();
    }
}
