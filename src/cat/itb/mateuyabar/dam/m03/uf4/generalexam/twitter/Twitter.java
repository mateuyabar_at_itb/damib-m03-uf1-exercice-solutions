package cat.itb.mateuyabar.dam.m03.uf4.generalexam.twitter;

import java.util.ArrayList;
import java.util.List;

public class Twitter {
    public static void main(String[] args) {
        List<Tweet> tweets = createTweets();
        pritn(tweets);
    }

    private static void pritn(List<Tweet> tweets) {
        for(Tweet tweet:tweets)
            tweet.print();
    }

    private static List<Tweet> createTweets() {
        List<Tweet> tweets = new ArrayList<>();
        Tweet tweet1 = new Tweet("iamdevloper", "07 de gener", "Remember, a few hours of trial and error can save you several minutes of looking at the README");
        tweets.add(tweet1);

        Tweet tweet2 = new Tweet("softcatala", "29 de març", "Avui mateix, #CommonVoiceCAT segueix creixent \uD83D\uDE80:\n\uD83D\uDDE3️856 hores enregistrades\n✅ 725 de validades.\nSi encara no has participat, pots fer-ho aquí!");
        tweets.add(tweet2);

        List<Choice> choices = new ArrayList<Choice>();
        choices.add(new Choice("Comèdia dramàtica - La Fúmiga"));
        choices.add(new Choice("In the night - Oques Grasses"));
        choices.add(new Choice("Una Lluna a l'Aigua - Txarango"));
        choices.add(new Choice("Esbarzers - La Gossa Sorda"));
        PollTweet pollTweet3 = new PollTweet("musicat", "02 d'abril", "Quina cançó t'agrada més?", choices);
        tweets.add(pollTweet3);

        voteTweet(pollTweet3);

        Tweet tweet4 = new Tweet("ProgrammerJokes", "05 d'abril", "Q: what's the object-oriented way to become weathy?\n\nA: Inheritance");
        tweets.add(tweet4);
        return tweets;
    }

    private static void voteTweet(PollTweet pollTweet3) {
        pollTweet3.vote(0);
        pollTweet3.vote(0);
        pollTweet3.vote(0);
        pollTweet3.vote(1);
        pollTweet3.vote(1);
        pollTweet3.vote(2);
        pollTweet3.vote(2);
        pollTweet3.vote(3);
        pollTweet3.vote(3);
        pollTweet3.vote(3);
        pollTweet3.vote(3);
    }
}
