package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class PushButtonPadlockSimulator {
    public static void main(String[] args) {
        boolean[] padlock = new boolean[8];
        Scanner scanner = new Scanner(System.in);
        int clickedButton = scanner.nextInt();
        while (clickedButton!=-1) {
            padlock[clickedButton] = !padlock[clickedButton];
            clickedButton = scanner.nextInt();
        }

        System.out.println(Arrays.toString(padlock));
    }
}
