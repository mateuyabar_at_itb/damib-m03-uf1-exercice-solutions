package cat.itb.mateuyabar.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class NiceIsLegalAge {
    public static void main(String[] args) {
        // Ask age to user
        Scanner scanner = new Scanner(System.in);
        int age = scanner.nextInt();

        // calculate is legal age (age >= 18)
        boolean isLegalAge = age >= 18;

        // imprimir legal si es major d'edat
        if(isLegalAge)
            System.out.println("ets major d'edat");

    }
}
