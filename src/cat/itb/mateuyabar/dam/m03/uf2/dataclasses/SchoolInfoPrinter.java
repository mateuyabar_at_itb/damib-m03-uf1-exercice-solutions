package cat.itb.mateuyabar.dam.m03.uf2.dataclasses;

import java.util.Scanner;

public class SchoolInfoPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        School school = School.readSchool(scanner);
        printSchool(school);
    }

    private static void printSchool(School school) {
        System.out.println(school);
    }


}
