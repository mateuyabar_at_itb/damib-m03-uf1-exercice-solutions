package cat.itb.mateuyabar.dam.m03.uf2.classfun;

public class Robot {
    double x;
    double y;
    double speed;

    public Robot(double x, double y, double speed) {
        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    public void goUp(){
        y+=speed;
    }

    public void goDown(){
        y-=speed;
    }

    public void goRight(){
        x+=speed;
    }

    public void goLeft(){
        x-=speed;
    }

    public void accelerate(){
        speed+=0.5;
    }

    public void slowDown(){
        speed-=0.5;
    }

    public void printSpeed(){
        System.out.printf(speed+"");
    }

    public void printPosition(){
        System.out.println(x+","+y);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getSpeed() {
        return speed;
    }
}
