package cat.itb.mateuyabar.dam.m03.uf1.arrays.exam;

import cat.itb.mateuyabar.dam.m03.uf1.arrays.ArrayReader;

import java.util.Scanner;

public class SumPositiveValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] values = ArrayReader.scannerReadIntArray(scanner);

        int sum = 0;
        for(int value : values){
            if(value>0)
                sum += value;
        }
        System.out.println(sum);
    }
}
