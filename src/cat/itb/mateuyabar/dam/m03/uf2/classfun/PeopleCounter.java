package cat.itb.mateuyabar.dam.m03.uf2.classfun;

import cat.itb.mateuyabar.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class PeopleCounter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> people = IntegerLists.readIntegerList(scanner);
        int sum = IntegerLists.sum(people);
        System.out.println(sum);
    }
}
