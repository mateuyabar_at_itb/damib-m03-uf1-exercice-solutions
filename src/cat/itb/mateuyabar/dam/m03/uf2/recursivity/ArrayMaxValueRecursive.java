package cat.itb.mateuyabar.dam.m03.uf2.recursivity;

import java.util.List;

public class ArrayMaxValueRecursive {
    public int max(List<Integer> array){
        return max(array, 0);
    }
    public int max(List<Integer> array, int i){
        if(i==array.size()-1)
            return array.get(i);
        int current = array.get(i);
        return Math.max(current, max(array, i+1));

    }
}
