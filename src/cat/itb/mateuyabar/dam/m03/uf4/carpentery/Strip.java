package cat.itb.mateuyabar.dam.m03.uf4.carpentery;

public class Strip extends CarpentryProduct{
    double length;

    public Strip(double unitPrice, double length) {
        super(unitPrice);
        this.length = length;
    }

    @Override
    protected double getAmount() {
        return length;
    }
}
