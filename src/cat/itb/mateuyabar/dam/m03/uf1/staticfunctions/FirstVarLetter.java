package cat.itb.mateuyabar.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class FirstVarLetter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char userChar = scanner.next().charAt(0);
        // lletra && minuscules
        if(Character.isLetter(userChar) && Character.isLowerCase(userChar)){
            System.out.println("Caràcter correcte.");
        } else {
            System.out.println("Caràcter incorrecte");
            if(Character.isLetter(userChar)){
                char correct = Character.toLowerCase(userChar);
                System.out.println(correct);
            }
        }
    }
}
