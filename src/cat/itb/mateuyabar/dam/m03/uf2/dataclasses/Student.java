package cat.itb.mateuyabar.dam.m03.uf2.dataclasses;

import java.util.List;
import java.util.Scanner;

public class Student {
    String name;
    School school;

    public Student(String name, School school) {
        this.name = name;
        this.school = school;
    }

    public static Student readStudent(Scanner scanner, List<School> schoolList) {
        int schoolNumber = scanner.nextInt();
        scanner.nextLine();
        String name = scanner.nextLine();
        School school = schoolList.get(schoolNumber);
        return new Student(name, school);

    }

    public String getName() {
        return name;
    }

    public School getSchool() {
        return school;
    }

    @Override
    public String toString() {
        return "--------------------\n"+getSchool()+"\n"+getName();
    }
}
