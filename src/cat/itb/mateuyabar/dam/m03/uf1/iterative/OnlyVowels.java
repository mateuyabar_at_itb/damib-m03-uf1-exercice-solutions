package cat.itb.mateuyabar.dam.m03.uf1.iterative;

import java.util.Scanner;

public class OnlyVowels {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int letterCount = scanner.nextInt();
        scanner.nextLine();

        for(int i =0; i<letterCount; ++i){
            char letter = scanner.nextLine().charAt(0);

            if(letter=='a' || letter=='e'|| letter=='i'|| letter=='o'|| letter=='u'){
                System.out.println(letter);
            }
        }
    }
}
