package cat.itb.mateuyabar.dam.m03.uf1.iterative;

import java.util.Scanner;

public class HowManyLines {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        String line = scanner.nextLine();
        while(! line.equals("END")){
            count ++;
            line = scanner.nextLine();
        }
    }
}
