package cat.itb.mateuyabar.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class HowManyDaysInMonth {
    public static void main(String[] args) {
        // Ask user for a month
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdueix un mes");
        int month = scanner.nextInt();
        int days;

        // calculate days in month
        switch (month){
            case 1: case 3: case 5: // TODO continue
                days = 31;
                break;
            case 2:
                days = 28;
                break;
            default:
                days = 30;
                break;
        }


        // print result
    }
}
