package cat.itb.mateuyabar.dam.m03.uf2.classfun;

import cat.itb.mateuyabar.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class MostDistantAge {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> ageList = IntegerLists.readIntegerList(scanner);
        int difference = difference(ageList);
        System.out.println(difference);
    }

    private static int difference(List<Integer> ageList) {
        int max = IntegerLists.max(ageList);
        int min = IntegerLists.min(ageList);
        return max-min;
    }
}
