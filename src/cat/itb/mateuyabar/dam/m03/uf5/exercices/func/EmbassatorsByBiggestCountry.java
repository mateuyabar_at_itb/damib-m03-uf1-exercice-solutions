package cat.itb.mateuyabar.dam.m03.uf5.exercices.func;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class EmbassatorsByBiggestCountry {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Country> countries = CountryDataSortByName.readCountries(scanner);
        List<Embassator> embassators = readEmbassators(scanner, countries);
        embassators.sort(Comparator.comparing(Embassator::getCountryName)
            .thenComparing(Embassator::getSurname)
            .thenComparing(Embassator::getName));
    }



    private static List<Embassator> readEmbassators(Scanner scanner, List<Country> countries) {
        int count = scanner.nextInt();
        List<Embassator> embassators = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Embassator embassator = readEmbassator(scanner, countries);
            embassators.add(embassator);
        }
        return embassators;
    }

    private static Embassator readEmbassator(Scanner scanner, List<Country> countries) {
        String name = scanner.nextLine();
        String surname = scanner.nextLine();
        String countryName = scanner.nextLine();
        Country country = countries.stream().filter(c -> c.name.equals(countryName)).findFirst().get();
        return new Embassator(name, surname, country);
    }
}
