package cat.itb.mateuyabar.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AnotherInverseOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = new ArrayList<>();

        int value = scanner.nextInt();
        while(value!=-1) {
            list.add(0, value);

            value = scanner.nextInt();
        }
        System.out.println(list);

    }
}
