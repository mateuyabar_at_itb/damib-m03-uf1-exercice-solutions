package cat.itb.mateuyabar.dam.m03.uf1.iterative;

import java.util.Scanner;

public class GoTournamentGreatestScore {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int bestScore = 0;
        String bestPlayer = "";

        String newPlayerName = scanner.nextLine();
        int newScore;

        while(!newPlayerName.equals("END")){
            newScore = scanner.nextInt();
            scanner.nextLine();

            if(newScore > bestScore){
                bestScore = newScore;
                bestPlayer = newPlayerName;
            }
            newPlayerName = scanner.nextLine();
        }
        System.out.println(bestPlayer+": "+bestScore);
    }
}
