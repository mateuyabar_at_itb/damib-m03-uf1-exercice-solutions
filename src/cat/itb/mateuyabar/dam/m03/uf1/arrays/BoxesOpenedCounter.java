package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class BoxesOpenedCounter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] boxes = new int[11];

        int box = scanner.nextInt();
        while (box!=-1) {
            boxes[box]++;
            box = scanner.nextInt();
        }

        System.out.println(Arrays.toString(boxes));
    }
}
