package cat.itb.mateuyabar.dam.m03.uf4.vehicles;

public class VehicleBrand {
    String name;
    String country;

    public VehicleBrand(String name, String country) {
        this.name = name;
        this.country = country;
    }

    @Override
    public String toString() {
        return "BycicleBrand{" +
                "name='" + name + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
