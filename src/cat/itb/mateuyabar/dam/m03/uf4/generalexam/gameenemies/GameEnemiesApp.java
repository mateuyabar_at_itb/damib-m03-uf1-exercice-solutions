package cat.itb.mateuyabar.dam.m03.uf4.generalexam.gameenemies;

import java.util.Arrays;
import java.util.List;

public class GameEnemiesApp {
    public static void main(String[] args) {
        List<Enemy> enemies = Arrays.asList(
                new Zombie("Zog", 10, "AARRRrrgg"),
                new Zombie("Lili", 30, "GRAaaArg"),
                new Troll("Jum", 12, 5),
                new Goblin("Tim", 60)
        );

        enemies.get(0).attack(5);
        enemies.get(0).attack(7);
        enemies.get(3).attack(7);
        enemies.get(1).attack(3);
        enemies.get(2).attack(4);
        enemies.get(2).attack(8);
        enemies.get(1).attack(4);
        enemies.get(0).attack(5);
        enemies.get(0).attack(1);
        enemies.get(2).attack(1);
        enemies.get(2).attack(1);
        enemies.get(2).attack(1);

        System.out.println(enemies);
    }
}
