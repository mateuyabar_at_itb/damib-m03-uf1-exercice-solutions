package cat.itb.mateuyabar.dam.m03.uf4.vehicles;

public class BicycleModel extends VehicleModel{
    int gears;

    public BicycleModel(String name, VehicleBrand brand, int gears) {
        super(name, brand);
        this.gears = gears;
    }

    @Override
    public String toString() {
        return "BicycleModel{" +
                "name='" + name + '\'' +
                ", gears=" + gears +
                ", brand=" + brand +
                '}';
    }
}
