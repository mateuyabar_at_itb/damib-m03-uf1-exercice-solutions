package cat.itb.mateuyabar.dam.m03.uf1.iterative;

import java.util.Scanner;

public class DeleteMe {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        scanner.nextLine();// clear line break
        String b = scanner.nextLine();
        String c = scanner.nextLine();

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }
}
