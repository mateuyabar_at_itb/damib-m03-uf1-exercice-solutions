package cat.itb.mateuyabar.dam.m03.uf5.exercices.func;

import cat.itb.mateuyabar.dam.m03.uf4.samples.Rectangle;

import java.util.*;

public class Sample {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        List<Rectangle> rectangles = new ArrayList<>();
        double avg = rectangles.stream()
                .mapToDouble(Rectangle::getArea)
                .average().getAsDouble();

        rectangles.sort(Comparator.comparing(Rectangle::getArea));
        rectangles.sort(Comparator.comparing(Rectangle::getArea).reversed());

        rectangles.sort(Comparator.comparing(Rectangle::getWidth)
                .thenComparing(Rectangle::getHeight));


        List<Integer> list2 = Arrays.asList(2,9,7,6,10,11);

        int max = list2.stream().reduce(0, (a, b) -> Math.max(a,b));

        /* 2 9 7,6,10,11
        0  2 9 9 9 10 11
         */
    }
}
