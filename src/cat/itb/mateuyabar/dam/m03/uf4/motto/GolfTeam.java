package cat.itb.mateuyabar.dam.m03.uf4.motto;

public class GolfTeam extends Team{
    String player;

    public GolfTeam(String name, String motto, String player) {
        super(name, motto);
        this.player = player;
    }

    @Override
    public void shoutMotto() {
        System.out.println(player+" "+motto);
    }
}
