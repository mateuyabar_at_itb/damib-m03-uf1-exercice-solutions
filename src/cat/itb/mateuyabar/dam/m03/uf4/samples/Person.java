package cat.itb.mateuyabar.dam.m03.uf4.samples;

public class Person {
    String name;
    DayOfWeek birthDayOfWeek;

    public Person(String name, DayOfWeek birthDayOfWeek) {
        this.name = name;
        this.birthDayOfWeek = birthDayOfWeek;
    }

}
