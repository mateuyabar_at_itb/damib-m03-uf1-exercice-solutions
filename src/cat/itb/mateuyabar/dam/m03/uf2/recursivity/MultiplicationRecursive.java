package cat.itb.mateuyabar.dam.m03.uf2.recursivity;

public class MultiplicationRecursive {
    public int multiply(int n, int m){
        if(m==1)
            return n;
        return n + multiply(n,m-1);
    }
}
